import logo from './logo.svg';
import './App.css';
import { useState } from 'react';

function App() {
const[input,setInput]=useState('')
const[answer,setAnswer]=useState([]) 
  const clickhandler=()=>{
    fetch(`https://rhymebrain.com/talk?function=getRhymes&word=hello=${input}`)
    .then(response=>response.json())
    .then(data=>{
      if(data.length==0){
        alert('invalid input')
      }
      else{
        setAnswer(data)
      }
    })
    
  }
  
  return (
    <div className="App">
      <h4>Type word for find ryming</h4>
      <input type='text' value={input} onChange={(e)=>setInput(e.target.value)}></input>
      <button onClick={clickhandler}>Search</button>
      {answer.map(item=>{
        return <p key={item.word}>{item.word}</p>
      })}
    </div>
  );
}

export default App;
